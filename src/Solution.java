import java.util.Arrays;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int[] arr = new int[]{0,1,0,3,12};
        int []arr2 = new int[]{0,0,1};
        int []arr3 = new int[]{1,0,0,0,12};
        moveZeroes(arr);
    }

    private void moveZeroes(int[] arr) {
        int k=0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]!=0)
                arr[k++]=arr[i];
        }
        for(;k<arr.length;k++){
            arr[k]=0;
        }
        for (int i : arr) {
            System.out.println(i);
        }
    }
}
